import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;


import java.awt.Component;


import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Color;



public class Formulario {
	
	
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Formulario window = new Formulario();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Formulario() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		String [] Nombres_c = {"Pegaso","Zentorno","Mero","Loco","Taurus", "Laco","Lara","Loto","Marlo","Cu","Lautaro","Mino","Karoso",
				"Lino","Aria","Mano","Mena","Carl","Poderoso","Pancracsio"};
		
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(80, 80, 460, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		final JProgressBar progressBar_c1 = new JProgressBar();
		progressBar_c1.setForeground(Color.ORANGE);
		final JProgressBar progressBar_c2 = new JProgressBar();		
		progressBar_c2.setForeground(Color.ORANGE);
		final JProgressBar progressBar_c3 = new JProgressBar();
		progressBar_c3.setForeground(Color.ORANGE);
		final JLabel lblNewLabel_c1 = new JLabel("New label");		
		lblNewLabel_c1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		final JLabel lblNewLabel_c2 = new JLabel("New label");		
		lblNewLabel_c2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		final JLabel lblNewLabel_c3 = new JLabel("New label");
		lblNewLabel_c3.setFont(new Font("Tahoma", Font.PLAIN, 12));
	        
		JButton btnEmpezar = new JButton("Empezar carrera");
		btnEmpezar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnEmpezar.setForeground(Color.BLUE);
			
		final JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblNewLabel = new JLabel("por: richarteq@gmail.com");
		
		
		JLabel lblHttpssitesgooglecomsitejavaejercicios = new JLabel("https://sites.google.com/site/javaejercicios/");
		
		JLabel lblNewLabel_c1_1_1 = new JLabel("Carrera de Caballos");
		lblNewLabel_c1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblNewLabel_c1_1 = new JLabel("Descanso");
		lblNewLabel_c1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel descanso1 = new JLabel("*******");
		descanso1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel descanso2 = new JLabel("*******");
		descanso2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel descanso3 = new JLabel("*******");
		descanso3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel lblNewLabel_c1_1_2 = new JLabel("Tiempo");
		lblNewLabel_c1_1_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel tiempo01 = new JLabel("*******");
		tiempo01.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel tiempo02 = new JLabel("*******");
		tiempo02.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		JLabel tiempo03 = new JLabel("*******");
		tiempo03.setFont(new Font("Tahoma", Font.PLAIN, 12));
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel)
								.addComponent(lblHttpssitesgooglecomsitejavaejercicios))
							.addPreferredGap(ComponentPlacement.RELATED, 211, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblNewLabel_c3)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(progressBar_c3, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblNewLabel_c2)
									.addGap(10)
									.addComponent(progressBar_c2, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblNewLabel_c1)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_c1_1_1, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
										.addComponent(progressBar_c1, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))))
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(8)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
												.addComponent(descanso2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
												.addComponent(descanso3, GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE))
											.addGap(16))
										.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
											.addComponent(lblNewLabel_c1_1)
											.addGap(16))))
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(descanso1)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_c1_1_2, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
								.addComponent(tiempo01, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
								.addComponent(tiempo02, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
								.addComponent(tiempo03, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE))
							.addGap(11)))
					.addGap(21))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(49)
					.addComponent(btnEmpezar, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(198, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel_c1_1_1)
							.addGap(15)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(progressBar_c1, GroupLayout.DEFAULT_SIZE, 16, Short.MAX_VALUE)
										.addComponent(lblNewLabel_c1, Alignment.TRAILING))
									.addGap(20)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(lblNewLabel_c2)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(progressBar_c2, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
											.addGap(3))
										.addComponent(descanso2, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
									.addGap(22)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(progressBar_c3, GroupLayout.DEFAULT_SIZE, 15, Short.MAX_VALUE)
										.addComponent(lblNewLabel_c3)))
								.addComponent(descanso3, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(btnEmpezar, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
							.addGap(31))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_c1_1_2, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_c1_1))
							.addGap(15)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(tiempo01, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
								.addComponent(descanso1, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
							.addGap(19)
							.addComponent(tiempo02, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
							.addGap(20)
							.addComponent(tiempo03, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 363, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel)
					.addGap(8)
					.addComponent(lblHttpssitesgooglecomsitejavaejercicios)
					.addContainerGap())
		);
		
		final JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		frame.getContentPane().setLayout(groupLayout);
		
		
		btnEmpezar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int numero = 0;
				textArea.setText(null);
				//@r.escobedo
				//new ElHilo(Nombres_c[numero = (int)(Math.random()*20+1)], progressBar_c1, textArea, lblNewLabel_c1, tiempo01, descanso1).start();
				//new ElHilo(Nombres_c[numero = (int)(Math.random()*20+1)], progressBar_c2, textArea, lblNewLabel_c2, tiempo02, descanso2).start();
				//new ElHilo(Nombres_c[numero = (int)(Math.random()*20+1)], progressBar_c3, textArea, lblNewLabel_c3, tiempo03, descanso3).start();
			}
		});	
		
	}	
}
