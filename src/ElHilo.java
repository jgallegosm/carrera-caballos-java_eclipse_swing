import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;




public class ElHilo extends Thread {
	JProgressBar caballo;
	JTextArea mensajes;
	JLabel nombre;
	JLabel tiempo;
	JLabel descanso;
	long tiempo_T;
	static int FIN = 4;
	String nombre_str;
	
	/*
	
	public ElHilo(String str, JProgressBar jpb, JTextArea jta, JLabel jl, JLabel jl_t, JLabel jl_d) {
		super(str);
		this.caballo = jpb;
		this.mensajes = jta;
		this.nombre = jl;
		this.tiempo=jl_t;
		this.descanso=jl_d;
		this.caballo.setMinimum(0);
		this.caballo.setMaximum(FIN);		
		this.nombre.setText(str);
		
	}
	
	*/
	
	// JL_D REPRESENTA EL TIEMPO DE DESCANSO QUE TIENE CADA CABALLO -> HECHO POR FRANS GALLEGOS


	// @jhuallpard: "LJabel jl_t Representa el tiempo que le toma al caballo terminar la carrera."
	//@SaraGabriela: jl representa el nombre del caballo
	public ElHilo(String str, JProgressBar jpb, JLabel jl, JLabel jl_t, JLabel jl_d) {
		super(str);
		this.caballo = jpb;
		this.nombre = jl;
		this.tiempo=jl_t;
		this.descanso=jl_d;
		this.caballo.setMinimum(0);
		this.caballo.setMaximum(FIN);		
		this.nombre.setText(str);
		this.tiempo_T=0;
		this.nombre_str = str;
		
	}
	
	
		
	public void run(){
		double times=0, descanso=0;
		long ti=System.currentTimeMillis();
		int aux=0;
		double acun=0;
		for (int i = 0; i < 7; i++) {
			//System.out.println("Posici�n " + i + ": " + getName());
			caballo.setValue(i);
			//mensajes.append("Posición " + i + ": " + getName() + "\n");
			
			try {
				aux=(int) (Math.random() * 2000);
				sleep(aux);
				acun+=aux;
				
				//mensajes.append("El caballo " + getName() + " descansa.\n");
				//System.out.println("El caballo " + getName() + " descansa.");
			} catch (InterruptedException e) {
				//System.out.println(e);
				//mensajes.append(String.valueOf(e)+"\n");
			}
		}
		long tf=System.currentTimeMillis();
        times =tf-ti;
        String tiemp= Double.toString(times);
        this.tiempo.setText(tiemp);
        String acuns= Double.toString(acun);
        this.descanso.setText(acuns);
        this.tiempo_T=(long) times;


	}
	
	
	public long getTiempo_T() {
		return tiempo_T;
	}
	
	public String get_nombre() {
		return nombre_str;
	}

	
}
