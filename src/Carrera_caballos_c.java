import java.awt.event.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.awt.*; 
import javax.swing.*; 

public class Carrera_caballos_c extends JFrame{
	public Carrera_caballos_c() {
	}

	static JFrame frame; 
	static JLabel label;
	static JButton start;
	static JButton run;
	static JTextField field;
	static JLabel ganador;
	static JLabel respuesta;
	
	 //private static Queue<ElHilo> queue;
	static ArrayList<ElHilo> caballos = new ArrayList<>();

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		frame = new JFrame("Carrera de Caballos");

		label = new JLabel("Ingrese el N�mero de Caballos");
		start = new JButton("Start");
		field = new JTextField(15);
		run = new JButton("RUN");
		ganador = new JLabel ("El ganador es: ");
		respuesta = new JLabel ();
		
/*
		//JOptionPane.showMessageDialog(null, "Esta es una carrera de caballos");
		
		String base = JOptionPane.showInputDialog("Ingrese el n�mero de caballos (debe estar: 1 < c < 21)");
		//String altura = JOptionPane.showInputDialog("Ingrese altura");
		
		//double A = (Double.parseDouble(base)*Double.parseDouble(altura))/2;
		Double.parseDouble(base);
		JOptionPane.showMessageDialog(null, "El n�mero ser�:  " + base);
		*/
		
		
		JPanel tempPanel = new JPanel();
		tempPanel.add(label);
		tempPanel.add(field);
		tempPanel.add(start);
		
		//queue = new LinkedList<ElHilo>();
		

		
		start.addActionListener(new ActionListener() {
	        
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int maxi= Integer.parseInt(field.getText()); 
				
				JPanel horsePanel = new JPanel();
				horsePanel.setLayout(new GridLayout(0,4));
				
				frame.remove(tempPanel);				
				
				JLabel l1 = new JLabel("Caballo");
				JLabel l2 = new JLabel("Posici�n");
				JLabel l3 = new JLabel("Tiempo Ejecuci�n");
				JLabel l4 = new JLabel("Tiempo Descanso");
				
				Font font = new Font("Comic Sans", Font.BOLD,12);
						
				l1.setFont(font);
				l2.setFont(font);
				l3.setFont(font);
				l4.setFont(font);

				
				l1.setHorizontalAlignment(JLabel.CENTER);
				l2.setHorizontalAlignment(JLabel.CENTER);
				l3.setHorizontalAlignment(JLabel.CENTER);
				l4.setHorizontalAlignment(JLabel.CENTER);
				
				horsePanel.add(l1);
				horsePanel.add(l2);
				horsePanel.add(l3);
				horsePanel.add(l4);	
				
				

/*
					ArrayList<Thread> caballos = new ArrayList<>();		
					for(int i=0;i<maxi;i++) {
						caballos.add( new OtroHilo("abc") );
					}		
					Iterator<Thread> iter = caballos.iterator(); 		
					while (iter.hasNext()) { 
			            iter.next().start(); 
			        } 		
				}*/
				
				
				
				
				String [] Nombres_c = {"Pegaso","Zentorno","Mero","Loco","Taurus", "Laco","Lara","Loto","Marlo","Cu","Lautaro","Mino","Karoso",
						"Lino","Aria","Mano","Mena","Carl","Poderoso","Pancracsio"};
				int numero;
				for(int m=0;m < maxi;m++){
									
					JProgressBar progressBar = new JProgressBar();
					progressBar.setMaximum(500);
					
					JLabel jl_nombre = new JLabel ("0");
					JLabel RunTime = new JLabel("0");
					JLabel SleepTime = new JLabel("0");
					RunTime.setHorizontalAlignment(JLabel.CENTER);
					SleepTime.setHorizontalAlignment(JLabel.CENTER);
					numero = 0;
					//ElHilo caballo = new ElHilo(Nombres_c[numero = (int)(Math.random()*20+1)],progressBar, jl_nombre, RunTime,SleepTime);
					caballos.add( new ElHilo(Nombres_c[numero = (int)(Math.random()*19+1)],progressBar, jl_nombre, RunTime,SleepTime));

					//JLabel nombreCaballo = new JLabel(caballo.getNombre());				
					//caballos.add(caballo);
									
					
					horsePanel.add(jl_nombre);
					horsePanel.add(progressBar);
					horsePanel.add(RunTime);
					horsePanel.add(SleepTime);
					
					frame.getContentPane().add(horsePanel);
					
				}
				
		
				frame.setSize(700,100+maxi*20);
				frame.revalidate();
			
				horsePanel.add(run);
				horsePanel.add(ganador);
				horsePanel.add(respuesta);
				frame.getContentPane().add(horsePanel);
				
				frame.revalidate();
				
			}          
	      });
		
		run.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				Iterator<ElHilo> iter = caballos.iterator(); 

				
				int len = caballos.size();
				for (int i = 0; i < len; i++) {
				  ElHilo ele = caballos.get(i);
				  ele.start();
				}
				
				ArrayList<ElHilo> tiempos_caballos = new ArrayList<ElHilo>();
				
				int muertos=0;	
				while(true) {
					
									
					for (int i = 0; i < len; i++) {
						
						if(!caballos.get(i).isAlive() && !caballos.get(i).isInterrupted()) {
							muertos+=1;
							tiempos_caballos.add(caballos.get(i));
						}
						System.out.println("LOS MUERTOS " + muertos);
						}
					
					
					if (muertos==caballos.size()) {
						break;
					}
				}
		
				
				String ganador_str = tiempos_caballos.get(0).get_nombre();
				System.out.println("EL GANADORRRR ES:  " + ganador_str);
				respuesta.setText(ganador_str);
				
				
			}
		});
		
		frame.getContentPane().add(tempPanel);
		
		frame.setSize(500, 80);
		frame.show();
		
	}
		
	}